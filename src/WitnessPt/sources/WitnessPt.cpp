#include "WitnessPt.hpp"
// Constructor
WitnessPt::WitnessPt()
{
    witptID = 0;
    elemID = 0;
    coords = {0.0, 0.0, 0.0};
    coordsIso = {0.0, 0.0, 0.0};
}

// Destructor
WitnessPt::~WitnessPt()
{
    // Delete the vectors
    coords.clear();
    coordsIso.clear();
}

// Getters
uint64_t WitnessPt::getNdims()
{
    return nDims;
}

uint64_t WitnessPt::getWitptID()
{
    return witptID;
}

uint64_t WitnessPt::getElemID()
{
    return elemID;
}

std::vector<float> WitnessPt::getCoords()
{
    return coords;
}

std::vector<float> WitnessPt::getCoordsIso()
{
    return coordsIso;
}

void WitnessPt::getData(float* d, float* p, float* t, std::vector<float> v)
{
    *d = data.getDensity();
    *p = data.getPressure();
    *t = data.getTemperature();
    v = data.getVelocity();
}

// Setters
void WitnessPt::setNdims(uint64_t n)
{
    nDims = n;
}

void WitnessPt::setWitptID(uint64_t i)
{
    witptID = i;
}

void WitnessPt::setElemID(uint64_t i)
{
    elemID = i;
}

void WitnessPt::setCoords(std::vector<float> c)
{
    coords = c;
}

void WitnessPt::setCoordsIso()
{
    // TODO: implement the method
    // Since the method is not implemented, fail the test and abort the program
    std::cerr << "ERROR:  method setCoordsIso() not implemented yet" << std::endl;
    std::abort();
}

void WitnessPt::setData(float d, float p, float t, std::vector<float> v)
{
    data.setDensity(d);
    data.setPressure(p);
    data.setTemperature(t);
    data.setVelocity(v);
}