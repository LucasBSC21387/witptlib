#include "WitptData.hpp"

// Destructor
WitptData::~WitptData()
{
    // Delete the vector
    velocity.clear();
}

// Getters
float WitptData::getDensity()
{
    return density;
}

float WitptData::getPressure()
{
    return pressure;
}

float WitptData::getTemperature()
{
    return temperature;
}

std::vector<float> WitptData::getVelocity()
{
    return velocity;
}

// Setters
void WitptData::setDensity(float d)
{
    density = d;
}

void WitptData::setPressure(float p)
{
    pressure = p;
}

void WitptData::setTemperature(float t)
{
    temperature = t;
}

void WitptData::setVelocity(std::vector<float> v)
{
    velocity = v;
}