#ifndef WITNESSPT_HPP
#define WITNESSPT_HPP

#include <vector>
#include <cstdint>
#include <iostream>
#include "WitptData.hpp"

class WitnessPt
{
    // Attributes
    private:
        uint64_t nDims;               // Number of physical dimensions
        uint64_t witptID;             // ID of the witness point
        uint64_t elemID;              // ID of the element containing the witness point
        std::vector<float> coords;    // Real coordinates of the witness point
        std::vector<float> coordsIso; // Isopar coordinates of the witness point
        WitptData data;               // Data at the witness point
    // Methods
    private:
        // Newton method for computing the isoparametric coordinates
        // TODO: implement this
    public:
        // Constructor
        WitnessPt();
        // Destructor
        ~WitnessPt();
        // Getters
        uint64_t getNdims();
        uint64_t getWitptID();
        uint64_t getElemID();
        std::vector<float> getCoords();
        std::vector<float> getCoordsIso();
        void getData(float* d, float* p, float* t, std::vector<float> v);
        // Setters
        void setNdims(uint64_t n);
        void setWitptID(uint64_t i);
        void setElemID(uint64_t i);
        void setCoords(std::vector<float> c);
        void setCoordsIso(); // TODO: figure out the inputs to this method
        void setData(float d, float p, float t, std::vector<float> v);
        // File readers
        // TODO: implement the methods
        // File writers
        // TODO: implement the methods
};

#endif // WITNESSPT_HPP