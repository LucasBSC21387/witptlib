#ifndef WITPTDATA_HPP
#define WITPTDATA_HPP

#include <vector>
#include <cstdint>

class WitptData
{
    // Attributes
    private:
        float density;               // Density
        float pressure;              // Pressure
        float temperature;           // Temperature
        std::vector<float> velocity; // Velocity
    // Methods
    private:
    public:
        // Destructor
        ~WitptData();
        // Getters
        float getDensity();
        float getPressure();
        float getTemperature();
        std::vector<float> getVelocity();
        // Setters
        void setDensity(float d);
        void setPressure(float p);
        void setTemperature(float t);
        void setVelocity(std::vector<float> v);
};

#endif // WITPTDATA_HPP