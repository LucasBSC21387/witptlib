#include <iostream>
#include <vector>
#include <cstdint>
#include <string>
#include <fstream>
#include <sstream>
#include "WitnessPt.hpp"

int main(void)
{
    // Set number of witness points
    const uint64_t nWits = 2;

    // Read the mesh file containing the necessary elements
    // TTODO: read the mesh file annd create element properties

    // Create a vector of witness points
    std::vector<WitnessPt> wits(nWits);

    // For each witness, generate a set or random coordinates and set the witness data
    // TODO: read a file containing this data
    for (uint64_t i = 0; i < nWits; i++)
    {
        // Set the witness point ID
        wits[i].setWitptID(i);

        // Set the number of dimensions
        wits[i].setNdims(3);

        // Set the coordinates
        std::vector<float> coords = {1.0, 2.0, 3.0};
        wits[i].setCoords(coords);

        // Compute the isoparametric coordinates of the witness point
        // TODO: implement the method properly
        wits[i].setCoordsIso();

        // Set the data
        // TODO: implement this
    }

    return 0;
}