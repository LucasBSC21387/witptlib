# Set the minimum CMake version required
cmake_minimum_required(VERSION 3.20)

# Set the project name and languages
project(witptLib LANGUAGES CXX)

# Set the default build type to Release
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

# Set the C++ standard to C++17
set(CMAKE_CXX_STANDARD 17)

# Set the library type to SHARED
set(LIBRARY_TYPE SHARED)

# Set the CXX flags
## If compiler is GNU
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
	## Common flags
	set(CMAKE_CXX_FLAGS "-Wall")
	## Debug flags
	set(CMAKE_CXX_FLAGS_DEBUG "-Wextra -Wpedantic -Werror -O0 -g")
	## Release flags
	set(CMAKE_CXX_FLAGS_RELEASE "-O3")
endif()

# use, i.e. don't skip the full RPATH for the build tree
set(CMAKE_SKIP_BUILD_RPATH FALSE)

# when building, don't use the install RPATH already
# (but later on when installing)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# the RPATH to be used when installing, but only if it's not a system directory
list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
endif("${isSystemDir}" STREQUAL "-1")

# Add the src folder
add_subdirectory(src)

# EEnable testing and add the test folder
enable_testing()
add_subdirectory(tests)